// Create an array for current users
var users = [];
var userName = [];

// Create a client instance
client = new Paho.MQTT.Client("live.heartos.org", Number(9001), makeid(10));

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;
client.reconnect = true;

function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

// connect the client
client.connect({onSuccess:onConnect});
function live_streaming(clicked_id){
    var live_userid = users[clicked_id.parentNode.parentNode.rowIndex];
    var live_userName = userName[clicked_id.parentNode.parentNode.rowIndex];
    console.log(live_userid);
    var live_url = "live_plot.html?userid="+live_userid+"&userName="+live_userName;
    var liveWindow = window.open(live_url);
}

function live_ecg_streaming(clicked_id){
    var live_userid = users[clicked_id.parentNode.parentNode.rowIndex];
    var live_userName = userName[clicked_id.parentNode.parentNode.rowIndex];
    console.log(live_userid);
    var live_url = "live_streaming.html?userid="+live_userid+"&username="+live_userName;
    var liveWindow = window.open(live_url);
}


// called when the client connects
function onConnect() {
  // Once a connection has been made, make a subscription and send a message.
  console.log("onConnect");
  client.subscribe("fourthfrontier/+/live-broadcast");
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}

// Function to add new row in the table
function addNewRow(rowCount, userID, derivedData){

    var table = document.getElementById("userTable");
    var row = table.insertRow(rowCount);
    row.className = "row100 head"

    var button = document.createElement("button");
    button.innerHTML = "Live Plot";
    button.type = "button";

    var button_live_streaming = document.createElement("button");
    button_live_streaming.innerHTML = "Live ECG";
    button_live_streaming.type = "button";

    var cell0 = row.insertCell(0);
    cell0.className = "cell100 column1";
    cell0.innerHTML = derivedData.userName;

	
    var cell1 = row.insertCell(1);
    cell1.className = "cell100 column2";
   
    var value = parseInt(derivedData.hearRate);

    // change the color of the text according to the heartrate zone	
    if(value >= 180){
	cell1.style.color = 'red';
    }else if (value >= 150){
	cell1.style.color = 'orange';
    }else if(value >= 120){
	cell1.style.color = '#ffd966';	
    }else if(value >= 90){
        cell1.style.color = 'green';
    }else if(value < 90){
	cell1.style.color = 'blue';
    }

    cell1.innerHTML = derivedData.hearRate;

    var cell2 = row.insertCell(2);
    cell2.className = "cell100 column3";
    
    var value = parseInt(derivedData.breathingRate);
    // change the color of the text according to the breathing rate zone
    if(value >= 50){
	cell2.style.color = 'red';
    }else if (value >= 40){
	cell2.style.color = 'orange';
    }else if(value >= 30){
	cell2.style.color = '#ffd966';	
    }else if(value >= 20){
	cell2.style.color = 'green';
    }else if(value < 20){
	cell2.style.color = 'blue';
    }

    cell2.innerHTML = derivedData.breathingRate;

    var cell3 = row.insertCell(3);
    cell3.className = "cell100 column4";
    
    var value = Math.abs(parseFloat(derivedData.strain.toFixed(3)));
	
    if(value >= 0.2){
	cell3.style.color = 'red';
    }else if (value >= 0.15){
	cell3.style.color = 'orange';
    }else if(value >= 0.10){
	cell3.style.color = '#ffd966';	
    }else if(value >= 0.05){
	cell3.style.color = 'green';
    }else if(value < 0.05){
	cell3.style.color = 'blue';
    }

    cell3.innerHTML = derivedData.strain.toFixed(3);

    var cell4 = row.insertCell(4);
    cell4.className = "cell100 column5";
    cell4.innerHTML = derivedData.average_qtc;
 
    var cell5 = row.insertCell(5);
    cell5.className = "cell100 column6";
    cell5.innerHTML = derivedData.cadence;

    var cell6 = row.insertCell(6);
    cell6.className = "cell100 column7";
    cell6.innerHTML = derivedData.shock;

    var cell7 = row.insertCell(7);
    cell7.className = "cell100 column8";
    
    var value = parseInt(derivedData.qos);
    if(value <= 70){
	cell7.style.color = 'red';
    }else{
	cell7.style.color = '#808080';
    }
    cell7.innerHTML = derivedData.qos;

    var cell8= row.insertCell(8);
    cell8.className = "cell100 column9";
    cell8.appendChild(button);
    button.onclick = function(){live_streaming(this);};
    
    var cell9 = row.insertCell(9);
    cell9.className = "cell100 column10";
    cell9.appendChild(button_live_streaming);

    button_live_streaming.onclick = function(){live_ecg_streaming(this);};
}


function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}


// Update the new derived data information
function updateRow(nrow, userID, derivedData){
	var table = document.getElementById("userTable");
	table.rows[nrow].cells[0].innerHTML = derivedData.userName;

	var value = parseInt(derivedData.hearRate);

	// change the color of the text according to the heartrate zone	
	if(value >= 180){
		table.rows[nrow].cells[1].style.color = 'red';
	}else if (value >= 150){
		table.rows[nrow].cells[1].style.color = 'orange';
	}else if(value >= 120){
		table.rows[nrow].cells[1].style.color = '#ffd966';	
	}else if(value >= 90){
		table.rows[nrow].cells[1].style.color = 'green';
	}else if(value < 90){
		table.rows[nrow].cells[1].style.color = 'blue';
	}

        table.rows[nrow].cells[1].innerHTML = derivedData.hearRate;

	var value = parseInt(derivedData.breathingRate);
	// change the color of the text according to the breathing rate zone
	if(value >= 50){
		table.rows[nrow].cells[2].style.color = 'red';
	}else if (value >= 40){
		table.rows[nrow].cells[2].style.color = 'orange';
	}else if(value >= 30){
		table.rows[nrow].cells[2].style.color = '#ffd966';	
	}else if(value >= 20){
		table.rows[nrow].cells[2].style.color = 'green';
	}else if(value < 20){
		table.rows[nrow].cells[2].style.color = 'blue';
	}
	table.rows[nrow].cells[2].innerHTML = derivedData.breathingRate;

	var value = Math.abs(parseFloat(derivedData.strain.toFixed(3)));
	
	if(value >= 0.2){
		table.rows[nrow].cells[3].style.color = 'red';
	}else if (value >= 0.15){
		table.rows[nrow].cells[3].style.color = 'orange';
	}else if(value >= 0.10){
		table.rows[nrow].cells[3].style.color = '#ffd966';	
	}else if(value >= 0.05){
		table.rows[nrow].cells[3].style.color = 'green';
	}else if(value < 0.05){
		table.rows[nrow].cells[3].style.color = 'blue';
	}

	table.rows[nrow].cells[3].innerHTML = derivedData.strain.toFixed(3);
	table.rows[nrow].cells[4].innerHTML = derivedData.average_qtc;
	table.rows[nrow].cells[5].innerHTML = derivedData.cadence;
	table.rows[nrow].cells[6].innerHTML = derivedData.shock;
	
	var value = parseInt(derivedData.qos);
	if(value <= 70){
		table.rows[nrow].cells[7].style.color = 'red';
	}else{
		table.rows[nrow].cells[7].style.color = '#808080';
	}
	table.rows[nrow].cells[7].innerHTML = derivedData.qos;

}

// called when a message arrives
function onMessageArrived(message) {
  var message_received = JSON.parse(message.payloadString);
  var topic_name = message.destinationName;
  var userID = topic_name.split("/")[1];
  // Check the user ID exists else push it to the array list and create a new column
  var user_existance = users.includes(userID);
  console.log(user_existance);
  if( user_existance ){
    // Find the index of the userID, This will be the column number
    // Update the row
    let n = users.indexOf(userID);
    console.log(n);
    updateRow(n, userID, message_received);
  }else{
    // push the userID into the array
    users.push(userID);
    userName.push(message_received.userName);
    addNewRow(users.length-1, userID, message_received);
  }
}
